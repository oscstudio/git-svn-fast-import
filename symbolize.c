#include <execinfo.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
/// http://www.unix.com/man-page/freebsd/3/backtrace_symbols_fd/ BSD Compatible
void symbolizetrace(int sig) {
  void *addresses[1024];
  int n = backtrace(addresses, 1024);
  backtrace_symbols_fd(addresses, n, STDERR_FILENO);
  signal(sig, SIG_DFL);
  raise(sig);
}

void symbolize() {
  signal(SIGSEGV, symbolizetrace); // Invaild memory address
  signal(SIGABRT, symbolizetrace); // Abort signal
}
